#!/bin/sh

for f in "$1/usr/bin"/*; do
	[ -f "$f" ] || continue
	mv "$f" "${f%.py}"
done
